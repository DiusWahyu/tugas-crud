<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    public function index()
    {
        // mengambil data dari table tbl_siswa
        $mahasiswa = DB::table('mahasiswa')->get();

        // mengirim data siswa ke view index
        return view('index',['mahasiswa' => $mahasiswa]);
    }


        // method untuk menampilkan view form tambah mahasiswa
    public function tambah()
    {
        // memanggil view tambah
        return view('tambah');
    }

        // method untuk insert data ke table tbl_siswa
    public function store(Request $request)
    {
        // insert data ke table tbl_siswa
        DB::table('mahasiswa')->insert([
            'nama_mahasiswa' => $request->nama_mhs,
            'nim_mahasiswa' => $request->nim_mhs,
            'kelas_mahasiswa' => $request->kelas_mhs,
            'prodi_mahasiswa' => $request->prodi_mhs,
            'fakultas_mahasiswa' => $request->fakultas_mhs]
        );

        // alihkan halaman ke halaman mahasiswa
        return redirect('/mahasiswa');
    }


        // method untuk edit data siswa
    public function edit($id)
    { 
        // mengambil data siswa berdasarkan id yang dipilih
        $mahasiswa = DB::table('mahasiswa')->where('id',$id)->get(); 
        
        // passing data siswa yang didapat ke view edit.blade.php 
        return view('edit',['mahasiswa' => $mahasiswa]);
    }


        // update data siswa
    public function update(Request $request)
    {
        // update data siswa
        DB::table('mahasiswa')->where('id',$request->id)->update([

            'nama_mahasiswa' => $request->nama_mhs,
            'nim_mahasiswa' => $request->nim_mhs,
            'kelas_mahasiswa' => $request->kelas_mhs,
            'prodi_mahasiswa' => $request->prodi_mhs,
            'fakultas_mahasiswa' => $request->fakultas_mhs
        ]);

        // alihkan halaman ke halaman siswa
        return redirect('/mahasiswa');
    }


        // method untuk hapus data siswa
    public function hapus($id)
    {
        // menghapus data siswa berdasarkan id yang dipilih
        DB::table('mahasiswa')->where('id',$id)->delete();
        
        // alihkan halaman ke halaman siswa
        return redirect('/mahasiswa');
    }

}
